from django.test import TestCase, Client
from django.urls import resolve
from .views import accordion
from django.http import HttpRequest

class isi_accordion(TestCase):

    def test_story7_using_index_func(self):
        found = resolve('/story7')
        self.assertEqual(found.func, accordion)

    def test_template_used(self):
        response =Client().get('/story7')
        self.assertTemplateUsed(response, 'accordion.html')
    
    def test_html_contains(self):
        request = HttpRequest()
        response = accordion(request)
        html_response = response.content.decode('utf8')
        self.assertIn('My Activity', html_response)
        self.assertIn('Personal Experience', html_response)
        self.assertIn('Achievment', html_response)
        self.assertIn('Interest', html_response)

    def test_url_is_exist(self):
    	response=Client().get('/story7')
    	self.assertEqual(response.status_code, 200)
