from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from .views import halaman_pencarian, data_api
from django.http import HttpRequest

class pencarian_buku(TestCase):

    def test_story8_using_index_func(self):
        found = resolve('/caribuku/')
        self.assertEqual(found.func, halaman_pencarian)

    def test_template_used(self):
        response =Client().get('/caribuku/')
        self.assertTemplateUsed(response, 'pencarian.html')
    
    def test_html_contains(self):
        request = HttpRequest()
        response = halaman_pencarian(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Cari Buku', html_response)
        self.assertIn('Search..', html_response)
        self.assertIn('No', html_response)
        self.assertIn('Title', html_response)
        self.assertIn('Description', html_response)
        self.assertIn('Photo', html_response)

    def test_url_is_exist(self):
    	response=Client().get('/caribuku/')
    	self.assertEqual(response.status_code, 200)

class cari_buku(TestCase):
	def test_story8_nama_buku_using_index_func(self):
		found = resolve('/namabuku/')
		self.assertEqual(found.func, data_api)

	def test_url_is_exist(self):
		response=self.client.post('/namabuku/?q=frozen')
		self.assertEqual(response.status_code, 200)
		html_response = response.content.decode('utf8')
		self.assertIn('frozen', html_response)
		self.assertIn('title', html_response)
