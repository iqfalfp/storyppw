from django.shortcuts import render
import json
from django.http import HttpResponse
from django.http import JsonResponse
import requests
# Create your views here.
def halaman_pencarian(request):
	return render(request, 'pencarian.html')

def data_api(request):
	data = request.GET['q']
	url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + data
	hasil = requests.get(url_tujuan).content
	tampilkan = json.loads(hasil)
	print(tampilkan)
	return JsonResponse(tampilkan, safe=False)