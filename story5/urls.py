from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('hapus', views.hapus, name='hapus'),
    path('matkul', views.matkulku, name= 'matkul'),
    path('jadwal', views.jadwal, name='jadwal')
]
    