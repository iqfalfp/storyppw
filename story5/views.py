from django.shortcuts import render, redirect
from .forms import MatkulForm
from .models import matkul

# Create your views here.

def matkulku(request):
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
    form = MatkulForm()
    return render(request, 'home/isiform.html', {'form': form})

def hapus(request):
    hapusform = request.POST.get('input')
    matkul.objects.get(nama_matkul= hapusform).delete()
    return redirect('story5:jadwal')

def jadwal(request):
    form = MatkulForm()
    value = matkul.objects.all()
    return render(request, 'home/jadwal.html', {'form': form, 'value':value})