from django.db import models
Tahun_CHOICES = [
    ('2018/2019', 'Gasal 2018/2019'),
    ('2018/2019', 'Ganjil 2018/2019'),
    ('2019/2020', 'Gasal 2019/2020'),
    ('2019/2020', 'Ganjil 2019/2020'),
    ('2020/2021', 'Gasal 2020/2021'),
    ('2020/2021', 'Ganjil 2020/2021'),
]

class matkul(models.Model): 
    nama_matkul = models.CharField('Mata Kuliah', max_length = 30) 
    dosen = models.CharField('Nama Dosen', max_length=25) 
    sks = models.PositiveIntegerField()
    deskripsi = models.TextField(max_length=100, null=True)
    tahun = models.CharField('Tahun Ajaran', choices=Tahun_CHOICES, max_length=16)
    ruang = models.CharField(max_length=8)
    last_modified = models.DateTimeField(auto_now_add = True) 
   
        # renames the instances of the model 
        # with their title name 
    def __str__(self): 
        return self.nama_matkul