from django import forms 
from .models import matkul 
  
class MatkulForm(forms.ModelForm):  
    class Meta: 
        model = matkul
        fields = ('nama_matkul', 'dosen', 'sks', 'deskripsi', 'tahun', 'ruang')