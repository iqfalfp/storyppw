from django.db import models

class Kegiatan1(models.Model): 
    nama_peserta_kegiatan1 = models.CharField( max_length = 30) 

    def __str__(self): 
        return self.nama_peserta_kegiatan1


class Kegiatan2(models.Model):
    nama_peserta_kegiatan2 = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_peserta_kegiatan2


class Kegiatan3(models.Model):
    nama_peserta_kegiatan3 = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_peserta_kegiatan3
