from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('Kegiatanku', views.kegiatanku, name='kegiatan1')
    # path('Kegiatan2', views.kegiatan2, name='kegiatan2'),
    # path('Kegiatan3', views.kegiatan3, name='kegiatan3')
]
