from django.contrib import admin
from .models import Kegiatan1, Kegiatan2, Kegiatan3

# Register your models here.
admin.site.register(Kegiatan1)
admin.site.register(Kegiatan2)
admin.site.register(Kegiatan3)
