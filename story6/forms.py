from django import forms
from .models import Kegiatan1, Kegiatan2, Kegiatan3


class Form_Kegiatan1(forms.ModelForm):
    class Meta:
        model = Kegiatan1
        fields = ('nama_peserta_kegiatan1', )


class Form_Kegiatan2(forms.ModelForm):
    class Meta:
        model = Kegiatan2
        fields = ('nama_peserta_kegiatan2', )


class Form_Kegiatan3(forms.ModelForm):
    class Meta:
        model = Kegiatan3
        fields = ('nama_peserta_kegiatan3', )
