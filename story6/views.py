from django.shortcuts import render
from .models import Kegiatan1, Kegiatan2, Kegiatan3
from .forms import Form_Kegiatan1, Form_Kegiatan2, Form_Kegiatan3
# Create your views here.
def kegiatanku(request):
    if request.method == 'POST':
        isiform1 = Form_Kegiatan1(request.POST)
        if isiform1.is_valid():
            isiform1.save()

    isiform1 = Form_Kegiatan1()
    if request.method == 'POST':
        isiform2 = Form_Kegiatan2(request.POST)
        if isiform2.is_valid():
            isiform2.save()

    isiform2 = Form_Kegiatan2()
    if request.method == 'POST':
        isiform3 = Form_Kegiatan3(request.POST)
        if isiform3.is_valid():
            isiform3.save()

    isiform3 = Form_Kegiatan3()
    context= {'isiform1': isiform1, 'isiform2': isiform2, 'isiform3': isiform3}
    return render(request, 'tambahkegiatan.html', context)


# def kegiatan2(request):
#     if request.method == 'POST':
#         isiform2 = Form_Kegiatan2(request.POST)
#         if isiform2.is_valid():
#             isiform2.save()

#     isiform2 = Form_Kegiatan2()
#     return render(request, 'tambahkegiatan.html', {'isiform2': isiform2})


# def kegiatan3(request):
#     if request.method == 'POST':
#         isiform3 = Form_Kegiatan3(request.POST)
#         if isiform3.is_valid():
#             isiform3.save()

#     isiform3 = Form_Kegiatan3()
#     return render(request, 'tambahkegiatan.html', {'isiform3': isiform3})
