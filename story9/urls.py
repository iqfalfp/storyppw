from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.login_account, name='login'),
    path('home/', views.home, name='home'),
    path('logout/', views.logout_account, name='logout')
]